import { Component, OnInit, Input } from '@angular/core';
import { Pokemon } from '../pokemon';

@Component({
  selector: 'app-pokemon-profile-avatar',
  templateUrl: './pokemon-profile-avatar.component.html',
  styleUrls: ['./pokemon-profile-avatar.component.scss']
})
export class PokemonProfileAvatarComponent implements OnInit {

  @Input() pokemon: Pokemon;

  constructor() { }

  ngOnInit() {
  }

}
