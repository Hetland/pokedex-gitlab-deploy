import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PokemonListComponent } from './pokemon-list/pokemon-list.component';
import { PokemonListItemComponent } from './pokemon-list-item/pokemon-list-item.component';
import { PokemonDetailComponent } from './pokemon-detail/pokemon-detail.component';
import { PokemonProfileComponent } from './pokemon-profile/pokemon-profile.component';
import { PokemonStatsComponent } from './pokemon-stats/pokemon-stats.component';
import { PokemonMovesComponent } from './pokemon-moves/pokemon-moves.component';
import { PokemonProfileAvatarComponent } from './pokemon-profile-avatar/pokemon-profile-avatar.component';
import { PokemonSearchbarComponent } from './pokemon-searchbar/pokemon-searchbar.component';

@NgModule({
  declarations: [
    AppComponent,
    PokemonListComponent,
    PokemonListItemComponent,
    PokemonDetailComponent,
    PokemonProfileComponent,
    PokemonStatsComponent,
    PokemonMovesComponent,
    PokemonProfileAvatarComponent,
    PokemonSearchbarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
